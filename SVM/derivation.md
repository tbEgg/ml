# SVM计算过程

## 最大间隔分类器

首先引出超平面这一概念：在$p$维空间中，超平面是一个$p-1$维的平面仿射子空间，可用下面的方程来描述
$$
\boldsymbol{{b}^{'}} \boldsymbol{x} + a = 0,\;\boldsymbol{b} = (b_1,\cdots,b_p)^{'}
\tag{1}
$$
将超平面记为$(\boldsymbol{b}, a)$，则样本空间中的任意点$\boldsymbol{x} = (x_1,\cdots,x_p)^{'}$到该超平面的距离可写为
$$
d = \frac{|\boldsymbol{{b}^{'}} \boldsymbol{x} + a|}{||\boldsymbol{b}||}
$$

$p$维空间中，任何满足$(1)$的点都落在超平面上，剩余的点根据$\boldsymbol{{b}^{'}} \boldsymbol{x} + a$的符号被划分为两个子空间。这样，给定训练集$D = \{(\boldsymbol{x_1}, y_1),\cdots,(\boldsymbol{x_m}, y_m)\}, \; y_i \in \{-1,1\}$，“-1”和“1"代表点的类别，若是能找到一个超平面将两类点分别划分到两个子空间中，就做到了分类的目的。

假设分割超平面$(\boldsymbol{b}, a)$能将训练样本正确分类，则它具有如下性质：
$$
\begin{cases}
\boldsymbol{{b}^{'}} \boldsymbol{x} + a > 0,\quad 如果y_i = 1,\\[2ex]
\boldsymbol{{b}^{'}} \boldsymbol{x} + a < 0,\quad 如果y_i = -1,
\end{cases}
\quad i = 1,\cdots,m.
\tag{2}
$$
示意图如下：

![1551936937341](/home/tbegg/.config/Typora/typora-user-images/1551936937341.png)

一般来说，如果数据可以被一个超平面分隔开，那么事实上存在无数个这样的超平面，如下图所示：

![1551937078337](/home/tbegg/.config/Typora/typora-user-images/1551937078337.png)

所以接下来的问题就是如何从无数个超平面中合理地挑选出其中的一个。一个很自然的想法是最大间隔超平面，即离训练观测最远的那个分隔超平面。也就是说，首先计算出每个训练观测到一个特定的分割超平面的垂直距离，这些距离的最小值就是训练观测到超平面的距离，被称为间隔。最大间隔超平面就是间隔最大的那个分割超平面，即它能使得训练观测到这个分割超平面的间隔达到最大。

![1551937478688](/home/tbegg/.config/Typora/typora-user-images/1551937478688.png)

观察上图，可以发现有两个训练观测落在了虚线上，它们到最大间隔超平面的距离是一样的。它们就叫做支持向量。最大间隔超平面只由支持向量决定。现在从理论角度来分析如何从训练观测构建最大间隔超平面。

先将$(2)$改写为：
$$
\begin{cases}
\boldsymbol{{b}^{'}} \boldsymbol{x} + a \geq 1,\quad 如果y_i = 1,\\[2ex]
\boldsymbol{{b}^{'}} \boldsymbol{x} + a \leq -1,\quad 如果y_i = -1,
\end{cases}
\quad i = 1,\cdots,m.
\tag{3}
$$
这样改写是合理的，因为如果$(2)$成立的话，我们总可以找到一个缩放变换()使得$(3)$成立。此时我们可以得到下图，间隔大小为$1/||\boldsymbol b||$。另外，$(3)$可等价的写为$y_i(\boldsymbol{{b}^{'}} \boldsymbol{x_i} + a) \geq 1,\;,i = 1,\cdots,m$。

![1551938360810](/home/tbegg/.config/Typora/typora-user-images/1551938360810.png)

显然，为了最大化间隔，我们只需最小化$||\boldsymbol b||^2 = \boldsymbol{b^{'}b}$。于是，为了寻找最大间隔超平面，只需求解下面的优化问题：
$$
\begin{cases}
\min \limits_{\boldsymbol{b},a} \frac 12 ||\boldsymbol b||^2 \\[2ex]
s.t.\; y_i(\boldsymbol{{b}^{'}} \boldsymbol{x_i} + a) \geq 1,\;,i = 1,\cdots,m
\end{cases}
\tag{4}
$$
它是凸优化问题且约束条件是仿射的，其中$(\boldsymbol b ,\,a) \in R^{p+1}$，又注意到隐含条件为$y_i$的取值为$\pm1$，所以它满足$\bold{Slater}$条件，则强对偶性成立。所以我们可以通过求解它的拉格朗日对偶问题的得到原问题的最优值，通过求解$\bold{KKT}$条件得到两个问题的最优解。

$(4)$对应的拉格朗日对偶函数为：
$$
L(\boldsymbol b,a,\boldsymbol \alpha)=\frac 12 ||\boldsymbol b||^2 +  \sum_{i=1}^{m}{\alpha_i(1 - y_i(\boldsymbol{{b}^{'}} \boldsymbol{x_i} + a))},\; \boldsymbol \alpha=(\alpha_i,\cdots,\alpha_m)^{'}
\tag{5}
$$
令$L(\boldsymbol b,a,\boldsymbol \alpha)$对$\boldsymbol b$和$a$的偏导数为0可得：
$$
\begin{cases}
\boldsymbol b = \sum\limits_{i = 1}^{m}{\alpha_i y_i \boldsymbol x_i}, \\[2ex]
0 = \sum\limits_{i = 1}^{m}{\alpha_iy_i}
\end{cases}
\tag{6}
$$
从而得到$(4)$的对偶问题：
$$
\begin{cases}
\max \limits_{\boldsymbol \alpha} &\left\{\sum\limits_{i = 1}^{m}{\alpha_i} - \frac 12 \sum\limits_{i = 1}^{m}\sum\limits_{j = 1}^{m}{\alpha_i\alpha_jy_iy_j\boldsymbol{x_i^{'}x_j}} \right\} \\[2ex]
s.t. \quad &\sum\limits_{i = 1}^{m}{\alpha_iy_i} = 0 \\
&\alpha_i \geq 0,\; i = 1,\cdots,m
\end{cases}
\tag{7}
$$
解出$\boldsymbol \alpha$后，求出$\boldsymbol b$和$a$即可得到模型
$$
f(\boldsymbol x) =\boldsymbol{b^{'}x} + a = \sum_{i = 1}^{m}{{\alpha_iy_i\boldsymbol{x_i^{'}x}}} + a
\tag{8}
$$
相应的KKT条件为：
$$
\begin{cases}
\alpha_i \geq 0, \\[3ex]
y_if(\boldsymbol x_i) - 1 \geq 0,\\[3ex]
\alpha_i(y_if(\boldsymbol x_i) - 1) = 0
\end{cases}
\quad i = 1,\cdots\,m
\tag{9}
$$




## 支持向量分类器

在许多情况下，分割超平面可能并不存在，如下图：

![1551941993491](/home/tbegg/.config/Typora/typora-user-images/1551941993491.png)

即使分割超平面确实存在，但这样的分类器对观测个体也过于敏感，支持向量的稍微偏移便会影响最大间隔超平面的位置。为了提高分类器对单个观测分类的稳定性以及使大部分训练观测能更好地分类，我们可以考虑非完美分类的超平面分类器。也就是说，允许小部分训练观测被误分以保证分类器对其余大部分观测能实现更好的分类。

![1551942424288](/home/tbegg/.config/Typora/typora-user-images/1551942424288.png)

在上图中，在两个间隔面上和之间的点叫做支持向量。

支持向量分类器允许某些观测不满足约束$y_i(\boldsymbol{{b}^{'}} \boldsymbol{x_i} + a) \geq 1$，于是优化目标可以写为
$$
\min \limits_{\boldsymbol{b},a} \left\{\frac 12 ||\boldsymbol b||^2 + C \sum_{i = 1}^{m}l_{0/1}(y_i(\boldsymbol{{b}^{'}} \boldsymbol{x} + a) - 1) \right\}
\tag{10}
$$
其中$C > 0$是调节参数，$l_{0/1}$是“$0/1$是损失函数”：
$$
l_{0/1}(z) = 
\begin{cases}
1,\quad z < 0,\\
0,\quad z \geq 0
\end{cases}
$$
ddd