import numpy as np
import matplotlib.pyplot as plt
from random import choice

# 样本，x为坐标，y为类别
class Sample:
    def __init__(self, data):
        data_list = list(map(float, data.split()))
        d = len(data_list)
        self.y = int(data_list[d - 1])
        self.x = np.array(data_list[:d - 1])

# 读取样本数据，返回样本组成的列表
def loadData(data_set):
    samples = []
    with open(data_set) as data:
        for line in data:
            samples.append(Sample(line.strip()))
    return samples

# 计算核函数值
def calKernelVal(samples, kernelFunc):
    n          =    len(samples)
    kernel_val =    np.zeros([n, n])
    
    # 还可以节省空间
    for i in range(n):
        for j in range(i, n):
            kernel_val[i][j] = kernelFunc(samples[i].x, samples[j].x)
            kernel_val[j][i] = kernel_val[i][j]

    return kernel_val

# 计算各变量违反KKT条件的程度
# 返回违反的绝对值组成的列表
def violateKKT(alpha, labels, f, C, e):
    n = len(alpha)
    violate_KKT = [0] * n
    for i in range(n):
        v = labels[i] * f(i)
        if alpha[i] == 0:
            if v < 1:
                violate_KKT[i] = 1 - v
        elif 0 < alpha[i] and alpha[i] < C:
            violate_KKT[i] = abs(1 - v)
        else:
            if labels[i] * f(i) > 1:
                violate_KKT[i] = v - 1
    return violate_KKT

def selectIndex(index_a, n, E):
    E_diff = [abs(E(index_a) - E(i)) for i in range(n)]
    return E_diff.index(max(E_diff))

def innerLoop(index_a, alpha, labels, offset, kernel_val, C, e, E):
    num = 0 # alpha对修改次数
    # index_b没有好好选
    n = len(labels)
    for index_b in [selectIndex(index_a, n, E), choice(range(n))]:
        if index_b == index_a:
            continue
    
        alpha_a =   alpha[index_a]
        alpha_b =   alpha[index_b]
        label_a =   labels[index_a]
        label_b =   labels[index_b]
        E_a     =   E(index_a)
        E_b     =   E(index_b)
        k_aa    =   kernel_val[index_a][index_a]
        k_bb    =   kernel_val[index_b][index_b]
        k_ab    =   kernel_val[index_a][index_b]

        # 计算未修剪的alpha
        eta = k_aa + k_bb - 2 * k_ab
        if eta <= 0:
            continue
        alpha_b_unc = alpha_b + label_b * (E_a - E_b) / eta
              
        # 根据约束条件，求alpha的上下边界
        if label_a != label_b:
            lower_bound = max(0, alpha_b - alpha_a)
            upper_bound = min(C, C + alpha_b - alpha_a)
        else:
            lower_bound = max(0, alpha_b + alpha_a - C)
            upper_bound = min(C, alpha_b + alpha_a)
             
        # 更新alpha值
        new_alpha_b = min(max(lower_bound, alpha_b_unc), upper_bound)
        new_alpha_a = alpha_a + label_a * label_b * (alpha_b - new_alpha_b)
        # alpha_a没有修剪，这里略加处理
        if new_alpha_a < 0:
            continue
    
        # 参数几乎不变，则不更新
        if abs(new_alpha_b - alpha_b) < e:
            continue
          
        # 更新offset
        offset_a = -E_a - label_a * k_aa * (new_alpha_a - alpha_a) - label_b * k_ab * (new_alpha_b - alpha_b) + offset
        offset_b = -E_b - label_a * k_ab * (new_alpha_a - alpha_a) - label_b * k_bb * (new_alpha_b - alpha_b) + offset
        if new_alpha_a > e and new_alpha_a < C - e:
            offset = offset_a
        elif new_alpha_b > e and new_alpha_b < C - e:
            offset = offset_b
        else:
            offset = (offset_a + offset_b) / 2
          
        alpha[index_a] = new_alpha_a
        alpha[index_b] = new_alpha_b
        num = num + 1
    return num, offset

def SMOEntire(samples, C, e, max_iter, kernelFunc):
    kernel_val  =   calKernelVal(samples, kernelFunc)   # 核函数值矩阵
    n           =   len(kernel_val)                     # 样本数
    labels      =   [samples[i].y for i in range(n)]    # 样本类别
    alpha       =   np.zeros(n)
    offset      =   0

    # 判别函数f(x)
    f = lambda index: sum([alpha[i] * labels[i] * kernel_val[index][i] for i in range(n)]) + offset
    E = lambda index: f(index) - labels[index]

    iter_times  =   0       # 有效迭代次数
    entire      =   True    # 是否进行完整遍历的标志
    while iter_times < max_iter:
        pair_changed = 0 # 每次迭代过程中修改alpha的次数

        # 获得需要遍历的alpha的下标集(外循环的alpha)
        violate_KKT = np.array(violateKKT(alpha, labels, f, C, e))
        traver_index = np.where(violate_KKT > e)[0] # 违反了KKT条件的alpha下标集
        # 若全部满足KKT条件，说明已找到最优解
        print(iter_times, "--", traver_index, entire) 
        if len(traver_index) == 0:
            break
        if entire == False: # 非边界点遍历
            # 获得非边界点的下标(即在0到C之间的alpha)
            traver_index = np.where((violate_KKT > 0) & (alpha > 0) & (alpha < C))[0]
        for i in traver_index:
            num, offset = innerLoop(i, alpha, labels, offset, kernel_val, C, e, E)
            pair_changed = pair_changed + num
        
        # 完整遍历后进入非边界遍历
        # 非边界遍历若没有修改alpha，进入完整遍历
        if entire == True:
            if pair_changed == 0:
                break
            entire = False
        elif pair_changed == 0:
            entire = True
        
        # 有效遍历
        if pair_changed > 0:
            iter_times = iter_times + pair_changed
    return alpha, offset, kernel_val, labels

if __name__ == "__main__":
    C = 100
    e = 0.00001
    max_iter    =   2000
    data_set    =   "./dataset"
    samples     =   loadData(data_set)
    kernelFunc  =   lambda a, b: np.inner(a, b)
    
    alpha, offset, kernel_val, labels = SMOEntire(samples, C, e, max_iter, kernelFunc)
    
    print("alpha :", alpha)
    print("offset :", offset)

    # 绘制图像
    support_vector = [i for i in range(len(labels)) if alpha[i] > 0.0001]
    f = lambda index: offset + sum([alpha[i] * labels[i] * kernel_val[index][i] for i in support_vector])
    for i, v in enumerate(samples):
        # 标注出支持向量
        val = f(i)
        print(i, ":", v.y, v.x, val)
        # 支持向量
        if val >= -1 and val <= 1:
            plt.scatter(v.x[0], v.x[1], s = 150, marker = 'o', c = "None", edgecolor = "red")
        
        # 根据类别绘制样本点
        if v.y == 1:
            plt.scatter(v.x[0], v.x[1], c = "c")
        else:
            plt.scatter(v.x[0], v.x[1], c = "r", marker = "D")

    plt.show()
