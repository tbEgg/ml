import numpy as np
import matplotlib.pyplot as plt

# 样本，x为坐标，y为类别
class Sample:
    def __init__(self, data):
        data_list = list(map(float, data.split()))
        d = len(data_list)
        self.y = int(data_list[d - 1])
        self.x = np.array(data_list[:d - 1])

# 读取样本数据，返回样本组成的列表
def loadData(data_set):
    samples = []
    with open(data_set) as data:
        for line in data:
            samples.append(Sample(line.strip()))
    return samples

# 计算核函数值
def calKernelVal(samples, kernelFunc):
    n          =    len(samples)
    kernel_val =    np.zeros([n, n])
    
    # 愚蠢的做法，放着
    for i in range(n):
        for j in range(n):
            kernel_val[i][j] = kernelFunc(samples[i].x, samples[j].x)

    return kernel_val

# 计算各变量违反KKT条件的程度
# 返回违反的绝对值组成的列表
def violateKKT(alpha, labels, f, C, e):
    n = len(alpha)
    if abs(sum([alpha[i] * labels[i] for i in range(n)])) > e:
        return False
    for i in range(n):
        print(i, "turn::", alpha[i], labels[i], f(i))
        if 0 <= alpha[i] and alpha[i] < e:
            if labels[i] * f(i) < 1:
                return False
        elif e <= alpha[i] and alpha[i] < C - e:
            if labels[i] * f(i) != 1:
                return False
        else:
            if labels[i] * f(i) > 1:
                return False
    return True

def SMO(samples, C, e, max_iter, kernelFunc):
    kernel_val  =   calKernelVal(samples, kernelFunc)
    n           =   len(kernel_val)     # 样本数
    labels      =   [samples[i].y for i in range(n)]
    alpha       =   np.zeros(n)
    offset      =   0

    # 判别函数f(x)
    f = lambda index: sum([alpha[i] * labels[i] * kernel_val[index][i] for i in range(n) if alpha[i] > 0.0001]) + offset

    # 简易版的选取，没有用启发式
    iter_times = 0
    while iter_times < max_iter:
        iter_times = iter_times + 1
        for index_a in range(n):
            for index_b in range(index_a + 1, n):
                alpha_a =   alpha[index_a]
                alpha_b =   alpha[index_b]
                label_a =   labels[index_a]
                label_b =   labels[index_b]
                E_a     =   f(index_a) - label_a
                E_b     =   f(index_b) - label_b
                k_aa    =   kernel_val[index_a][index_a]
                k_bb    =   kernel_val[index_b][index_b]
                k_ab    =   kernel_val[index_a][index_b]


                # 计算未修剪的alpha
                eta = k_aa + k_bb - 2 * k_ab
                # eta <= 0时求出的是最大值，所以退出
                if eta <= 0:
                    continue
                alpha_b_unc = alpha_b + label_b * (E_a - E_b) / eta
                
                # 根据约束条件，求alpha的上下边界
                if label_a != label_b:
                    lower_bound = max(0, alpha_b - alpha_a)
                    upper_bound = min(C, C + alpha_b - alpha_a)
                else:
                    lower_bound = max(0, alpha_b + alpha_a - C)
                    upper_bound = min(C, alpha_b + alpha_a)
                
                # 更新alpha值
                new_alpha_b = min(max(lower_bound, alpha_b_unc), upper_bound)
                new_alpha_a = alpha_a + label_a * label_b * (alpha_b - new_alpha_b)
                # alpha_a没有修剪，这里略加处理
                if new_alpha_a < 0:
                    continue
            
                # 更新offset
                offset_a = -E_a - label_a * k_aa * (new_alpha_a - alpha_a) - label_b * k_ab * (new_alpha_b - alpha_b) + offset
                offset_b = -E_b - label_a * k_ab * (new_alpha_a - alpha_a) - label_b * k_bb * (new_alpha_b - alpha_b) + offset
                if new_alpha_a > e and new_alpha_a < C:
                    offset = offset_a
                elif new_alpha_b > e and new_alpha_b < C:
                    offset = offset_b
                else:
                    offset = (offset_a + offset_b) / 2

                # 参数几乎不变，则不更新
                if abs(new_alpha_b - alpha_b) < 0:
                    continue
                
                alpha[index_a] = new_alpha_a
                alpha[index_b] = new_alpha_b

    return alpha, offset, kernel_val, labels

if __name__ == "__main__":
    C = 100
    e = 0.0001
    max_iter    =   100
    data_set    =   "./dataset"
    samples     =   loadData(data_set)
    kernelFunc  =   lambda a, b: np.inner(a, b)
    
    alpha, offset, kernel_val, labels = SMO(samples, C, e, max_iter, kernelFunc)
    
    print("alpha :", alpha)
    print("offset :", offset)

    # 绘制图像
    support_vector = [i for i in range(len(labels)) if alpha[i] > 0.0001]
    f = lambda index: offset + sum([alpha[i] * labels[i] * kernel_val[index][i] for i in support_vector])
    for i, v in enumerate(samples):
        # 标注出支持向量
        val = f(i)
        print(i, ":", v.y, v.x, val)
        # 支持向量
        if val >= -1 and val <= 1:
            plt.scatter(v.x[0], v.x[1], s = 150, marker = 'o', c = "None", edgecolor = "red")
        
        # 根据类别绘制样本点
        if v.y == 1:
            plt.scatter(v.x[0], v.x[1], c = "c")
        else:
            plt.scatter(v.x[0], v.x[1], c = "r", marker = "D")

    plt.show()
